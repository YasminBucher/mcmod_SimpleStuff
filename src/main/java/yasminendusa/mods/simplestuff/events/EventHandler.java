package yasminendusa.mods.simplestuff.events;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.FurnaceRecipes;
import net.minecraft.util.EnumHand;
import net.minecraftforge.event.entity.player.PlayerEvent;
import net.minecraftforge.event.world.BlockEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import yasminendusa.mods.simplestuff.items.ItemMultiTool;
import yasminendusa.mods.simplestuff.items.modules.upgrademodules.ItemAutosmeltModule;
import yasminendusa.mods.simplestuff.items.modules.upgrademodules.ItemDigSpeedModule;

import java.util.ListIterator;
import java.util.Random;

public class EventHandler{
	protected static final Random random = new Random();
	
	@SubscribeEvent
	public void onBlockEventHarvestDropsEvent(BlockEvent.HarvestDropsEvent e){
		if(e.getHarvester() == null){
			return;
		}
		
		ItemStack heldStack = e.getHarvester().getHeldItem(EnumHand.MAIN_HAND);
		
		if(heldStack == null){
			return;
		}
		
		Item heldItem = heldStack.getItem();
		
		if(heldItem == null){
			return;
		}
		
		if(!(heldItem instanceof ItemMultiTool) || !((ItemMultiTool)heldItem).isModuleInstalled(heldStack, ItemAutosmeltModule.class)){
			return;
		}
		
		ListIterator<ItemStack> drops = e.getDrops().listIterator();
		
		while(drops.hasNext()){
			ItemStack drop = drops.next();
			ItemStack dropSmelted = FurnaceRecipes.instance().getSmeltingResult(drop);
			
			if(dropSmelted != null){
				if(dropSmelted.getCount() > 0){
					dropSmelted = dropSmelted.copy();
					
					dropSmelted.setCount(drop.getCount());
					
					if(e.getFortuneLevel() > 0){
						dropSmelted.setCount(dropSmelted.getCount() * random.nextInt(e.getFortuneLevel() + 1) + 1);
					}
					
					drops.set(dropSmelted);
					
					int xp = (int)FurnaceRecipes.instance().getSmeltingExperience(dropSmelted);
					
					if(xp > 0){
						e.getState().getBlock().dropXpOnBlockBreak(e.getWorld(), e.getPos(), xp);
					}
				}
			}
		}
	}
	
	@SubscribeEvent
	public void onPlayerEventBreakSpeed(PlayerEvent.BreakSpeed event){
		ItemStack heldStack = event.getEntityPlayer().getHeldItem(EnumHand.MAIN_HAND);
		Item heldItem = heldStack.getItem();
		
		if(heldItem instanceof ItemMultiTool){
			ItemStack digSpeedModuleStack = ((ItemMultiTool)heldItem).getInstalledModule(heldStack, ItemDigSpeedModule.class);
			
			if(digSpeedModuleStack != null){
				event.setNewSpeed(event.getOriginalSpeed() * (digSpeedModuleStack.getMetadata() + 2));
			}
		}
	}
}