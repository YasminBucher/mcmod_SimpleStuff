package yasminendusa.mods.simplestuff;

import net.minecraftforge.common.config.Config;

@Config(modid = Constants.MODID)
public class MultiToolConfig{
	@Config.Comment(value = "General configuration.")
	public static GeneralConfig generalConfig = new GeneralConfig();
	
	@Config.Comment(value = "Enabled/Disabled items")
	public static ItemsEnabledConfig itemsEnabledConfig = new ItemsEnabledConfig();
	
	@Config.Comment(value = "Energy Usage")
	public static EnergyUsageConfig energyUsageConfig = new EnergyUsageConfig();
	
	public static class GeneralConfig{
		@Config.Comment(value = "Determines the base damage of the multitool without the sword module installed.")
		public float baseDamage = 0.0f;
		
		@Config.Comment(value = "Determines the base damage of the sword module.")
		public float weaponDamage = 5.0f;
		
		@Config.Comment(value = "Determines the additional damage per level of the sword module.")
		public float weaponDamagePerLevel = 1.0f;
		
		@Config.Comment(value = "Determines the additional damage per level of the weapon damage module.")
		public float weaponDamageModuleAddonDamage = 2.0f;
	}
	
	public static class ItemsEnabledConfig{
		@Config.Comment(value = "This value determines whether or not the MultiTool should be enabled.")
		public boolean itemMultiTool = true;
		
		@Config.Comment(value = "This value determines whether or not the axe module should be enabled.")
		public boolean itemAxeModule = true;
		
		@Config.Comment(value = "This value determines whether or not the pickaxe module should be enabled.")
		public boolean itemPickaxeModule = true;
		
		@Config.Comment(value = "This value determines whether or not the shears module should be enabled.")
		public boolean itemShearsModule = true;
		
		@Config.Comment(value = "This value determines whether or not the shovel module should be enabled.")
		public boolean itemShovelModule = true;
		
		@Config.Comment(value = "This value determines whether or not the sword module should be enabled.")
		public boolean itemSwordModule = true;
		
		@Config.Comment(value = "This value determines whether or not the autosmelt module should be enabled.")
		public boolean itemAutosmeltModule = true;
		
		@Config.Comment(value = "This value determines whether or not the dig aoe module should be enabled.")
		public boolean itemDigAoeModule = true;
		
		@Config.Comment(value = "This value determines whether or not the digspeed module should be enabled.")
		public boolean itemDigSpeedModule = true;
		
		@Config.Comment(value = "This value determines whether or not the fortune module should be enabled.")
		public boolean itemFortuneModule = true;
		
		@Config.Comment(value = "This value determines whether or not the laser range module should be enabled.")
		public boolean itemLaserRangeModule = true;
		
		@Config.Comment(value = "This value determines whether or not the mining power module should be enabled.")
		public boolean itemMiningPowerModule = true;
		
		@Config.Comment(value = "This value determines whether or not the silktouch module should be enabled.")
		public boolean itemSilktouchModule = true;
		
		@Config.Comment(value = "This value determines whether or not the weapon damage module should be enabled.")
		public boolean itemWeaponDamageModule = true;
		
		@Config.Comment(value = "This value determines whether or not the combat laser module should be enabled.")
		public boolean itemCombatLaserModule = true;
		
		@Config.Comment(value = "This value determines whether or not the mining laser module should be enabled.")
		public boolean itemMiningLaserModule = true;
		
		@Config.Comment(value = "This value determines whether or not the MultiTool battery module be enabled.")
		public boolean itemBatteryModule = true;
	}
	
	public static class EnergyUsageConfig{
		@Config.RangeInt(min = 0)
		@Config.Comment(value = "This value determines how much energy will be used per block mined by default.")
		public int baseEnergyUsage = 50;
		
		@Config.RangeInt(min = 0)
		@Config.Comment(value = "This value determines how much energy will be used when shearing a sheep.")
		public int shearsEnergyUsage = 50;
		
		@Config.RangeInt(min = 0)
		@Config.Comment(value = "This value determines how much energy will be used when making a path with the shovel module.")
		public int shovelPathEnergyUsage = 50;
		
		@Config.RangeInt(min = 0)
		@Config.Comment(value = "This value determines how much additional energy will be used per block mined when the silktouch module is installed.")
		public int silktouchEnergyUsage = 100;
		
		@Config.RangeInt(min = 0)
		@Config.Comment(value = {"This value determines how much additional energy will be used per block mined when the fortune module is installed.", "This value is 'per level' of fortune."})
		public int fortuneEnergyUsage = 50;
		
		@Config.RangeInt(min = 0)
		@Config.Comment(value = "This value determines how much additional energy will be used per block mined when the autosmelt module is installed.")
		public int autosmeltEnergyUsage = 50;
	}
}