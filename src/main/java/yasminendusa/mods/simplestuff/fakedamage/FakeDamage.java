package yasminendusa.mods.simplestuff.fakedamage;

public class FakeDamage implements IFakeDamage{
	private int maxDurability;
	private int durability;
	
	public FakeDamage(int maxDurability){
		this.maxDurability = maxDurability;
	}
	
	public void setDurability(int durability){
		if(durability > this.maxDurability){
			durability = this.maxDurability;
		}
		
		if(durability < 0){
			durability = 0;
		}
		
		this.durability = durability;
	}
	
	@Override
	public int applyDamage(int damage){
		int actualDamage = Math.min(damage, this.durability);
		
		this.setDurability(this.durability - actualDamage);
		
		return actualDamage;
	}
	
	@Override
	public int repair(int repairAmount){
		int actualRepair = Math.min(this.maxDurability - this.durability, repairAmount);
		
		this.setDurability(this.durability + actualRepair);
		
		return actualRepair;
	}
	
	@Override
	public int getDurability(){
		return this.durability;
	}
	
	@Override
	public int getMaxDurability(){
		return this.maxDurability;
	}
	
	@Override
	public boolean canTakeDamage(){
		return true;
	}
	
	@Override
	public boolean canBeRepaired(){
		return false;
	}
}