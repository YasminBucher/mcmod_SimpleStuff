package yasminendusa.mods.simplestuff.fakedamage;

public interface IFakeDamage{
	int applyDamage(int damage);
	int repair(int repairAmount);
	int getDurability();
	int getMaxDurability();
	boolean canTakeDamage();
	boolean canBeRepaired();
}
