package yasminendusa.mods.simplestuff.fakedamage;

import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagInt;
import net.minecraft.util.EnumFacing;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityInject;
import net.minecraftforge.common.capabilities.CapabilityManager;

import java.util.concurrent.Callable;

public class CapabilityFakeDamage{
	@CapabilityInject(IFakeDamage.class)
	public static Capability<IFakeDamage> FAKEDAMAGE = null;
	
	public static void register(){
		CapabilityManager.INSTANCE.register(IFakeDamage.class,
				new Capability.IStorage<IFakeDamage>(){
					@Override
					public NBTBase writeNBT(Capability<IFakeDamage> capability, IFakeDamage instance, EnumFacing side){
						return new NBTTagInt(instance.getDurability());
					}
					
					@Override
					public void readNBT(Capability<IFakeDamage> capability, IFakeDamage instance, EnumFacing side, NBTBase nbt){
						if(!(instance instanceof FakeDamage)){
							throw new IllegalArgumentException("Can not deserealize to an instance that isn't the default implementation");
						}
						
						((FakeDamage)instance).setDurability(((NBTTagInt)nbt).getInt());
					}
				},
				new Callable<IFakeDamage>(){
					@Override
					public IFakeDamage call() throws Exception{
						return new FakeDamage(1000);
					}
				});
	}
}