package yasminendusa.mods.simplestuff.fakedamage;

import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumFacing;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.ICapabilityProvider;
import yasminendusa.mods.simplestuff.items.base.ItemToolModuleBase;

import javax.annotation.Nullable;

public class MultitoolFakeDamageCapabilityProvider implements ICapabilityProvider{
	public MultitoolFakeDamage fakeDamageStorage;
	
	public MultitoolFakeDamageCapabilityProvider(ItemStack stack, ItemToolModuleBase item){
		this.fakeDamageStorage = new MultitoolFakeDamage(stack, item);
	}
	
	@Override
	public boolean hasCapability(Capability<?> capability, @Nullable EnumFacing facing){
		if(capability == CapabilityFakeDamage.FAKEDAMAGE){
			return true;
		}
		
		return false;
	}
	
	@Override
	public <T> T getCapability(Capability<T> capability, @Nullable EnumFacing facing){
		if(capability == CapabilityFakeDamage.FAKEDAMAGE){
			return (T) this.fakeDamageStorage;
		}
		
		return null;
	}
}
