package yasminendusa.mods.simplestuff.fakedamage;

import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import yasminendusa.mods.simplestuff.items.ModItems;
import yasminendusa.mods.simplestuff.items.base.ItemToolModuleBase;

public class MultitoolFakeDamage implements IFakeDamage{
	ItemStack stack;
	ItemToolModuleBase item;
	
	public MultitoolFakeDamage(ItemStack stack, ItemToolModuleBase item){
		this.stack = stack;
		this.item = item;
	}
	
	public int getCurrentDurability(){
		if(stack.hasTagCompound()){
			int durability = stack.getTagCompound().getInteger("currentDurability");
			
			if(durability > this.getMaxDurability()){
				durability = this.getMaxDurability();
			}
			
			if(durability < 0){
				durability = 0;
			}
			
			return durability;
		}
		
		return 0;
	}
	
	public void setCurrentDurability(int currentDurability){
		if(!stack.hasTagCompound()){
			stack.setTagCompound(new NBTTagCompound());
		}
		
		if(currentDurability > this.getMaxDurability()){
			currentDurability = this.getMaxDurability();
		}
		
		if(currentDurability < 0){
			currentDurability = 0;
		}
		
		stack.getTagCompound().setInteger("currentDurability", currentDurability);
	}
	
	@Override
	public int applyDamage(int damage){
		int actualDamage = Math.min(damage, this.getCurrentDurability());
		
		this.setCurrentDurability(this.getDurability() - actualDamage);
		
		return actualDamage;
	}
	
	@Override
	public int repair(int repairAmount){
		int actualRepair = Math.min(this.getMaxDurability() - this.getCurrentDurability(), repairAmount);
		
		this.setCurrentDurability(this.getCurrentDurability() + actualRepair);
		
		return 0;
	}
	
	@Override
	public int getDurability(){
		return this.getCurrentDurability();
	}
	
	@Override
	public int getMaxDurability(){
		ItemToolModuleBase itemToolModuleBase = (ItemToolModuleBase)ModItems.getItemInstance(this.item.getClass());
		
		return itemToolModuleBase.getMaxDurability(this.stack.getMetadata());
	}
	
	@Override
	public boolean canTakeDamage(){
		return true;
	}
	
	@Override
	public boolean canBeRepaired(){
		return false;
	}
}