package yasminendusa.mods.simplestuff.gui;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.EnumHand;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.network.IGuiHandler;
import yasminendusa.mods.simplestuff.inventory.MultiToolInventory;
import yasminendusa.mods.simplestuff.items.ItemMultiTool;

public class MultiToolGuiHandler implements IGuiHandler {
    public static final int MULTITOOL_GUI = 0;

    @Override
    public Object getServerGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
        if(ID == MULTITOOL_GUI && player.getHeldItem(EnumHand.MAIN_HAND).getItem() instanceof ItemMultiTool) {
            return new MultiToolContainer(player.inventory, new MultiToolInventory(player.getHeldItem(EnumHand.MAIN_HAND)));
        }

        return null;
    }

    @Override
    public Object getClientGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
        if(ID == MULTITOOL_GUI && player.getHeldItem(EnumHand.MAIN_HAND).getItem() instanceof ItemMultiTool){
            return new MultiToolGuiContainer(player.inventory, new MultiToolInventory(player.getHeldItem(EnumHand.MAIN_HAND)));
        }

        return null;
    }
}