package yasminendusa.mods.simplestuff.gui;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import yasminendusa.mods.simplestuff.inventory.MultiToolInventory;

public class MultiToolContainer extends Container {
    private MultiToolInventory multiToolInventory;

    public MultiToolContainer(IInventory playerInv, MultiToolInventory multiToolInventory){
        this.multiToolInventory = multiToolInventory;
	
        // Multitool Inventory, Slot 0 - 15, Slot IDs 0 - 15
	    this.addSlotToContainer(new SingleItemSlot(this.multiToolInventory, 0, 18, 7));
	    this.addSlotToContainer(new SingleItemSlot(this.multiToolInventory, 1, 18, 27));
	    this.addSlotToContainer(new SingleItemSlot(this.multiToolInventory, 2, 18, 47));
	    this.addSlotToContainer(new SingleItemSlot(this.multiToolInventory, 3, 18, 67));
	    this.addSlotToContainer(new SingleItemSlot(this.multiToolInventory, 4, 18, 87));
	    this.addSlotToContainer(new SingleItemSlot(this.multiToolInventory, 5, 18, 107));
	    this.addSlotToContainer(new SingleItemSlot(this.multiToolInventory, 6, 18, 127));
	    this.addSlotToContainer(new SingleItemSlot(this.multiToolInventory, 7, 18, 147));
	    
	    this.addSlotToContainer(new SingleItemSlot(this.multiToolInventory, 8, 141, 7));
	    this.addSlotToContainer(new SingleItemSlot(this.multiToolInventory, 9, 141, 27));
	    this.addSlotToContainer(new SingleItemSlot(this.multiToolInventory, 10, 141, 47));
	    this.addSlotToContainer(new SingleItemSlot(this.multiToolInventory, 11, 141, 67));
	    this.addSlotToContainer(new SingleItemSlot(this.multiToolInventory, 12, 141, 87));
	    this.addSlotToContainer(new SingleItemSlot(this.multiToolInventory, 13, 141, 107));
	    this.addSlotToContainer(new SingleItemSlot(this.multiToolInventory, 14, 141, 127));
	    this.addSlotToContainer(new SingleItemSlot(this.multiToolInventory, 15, 141, 147));
        
        // Player Inventory, Slot 9 - 35, Slot IDs 9 - 35
        for (int y = 0; y < 3; y++) {
            for (int x = 0; x < 9; x++) {
                this.addSlotToContainer(new Slot(playerInv, x + y * 9 + 9, 7 + x * 18, 193 + y * 18));
            }
        }

        // Player Inventory, Slot 0 - 8, Slot IDs 36 - 44 (Hotbar)
        for (int x = 0; x < 9; x++) {
            this.addSlotToContainer(new Slot(playerInv, x, 7 + x * 18, 251));
        }
    }

    @Override
    public boolean canInteractWith(EntityPlayer playerIn) {
        return this.multiToolInventory.isUsableByPlayer(playerIn);
    }

    @Override
    public ItemStack transferStackInSlot(EntityPlayer playerIn, int fromSlot){
        ItemStack previous = ItemStack.EMPTY;
        Slot slot = this.inventorySlots.get(fromSlot);

        if(slot != null && slot.getHasStack()){
            ItemStack current = slot.getStack();
            previous = current.copy();

            if(fromSlot < MultiToolInventory.INV_SIZE){
                if(!this.mergeItemStack(current, MultiToolInventory.INV_SIZE, MultiToolInventory.INV_SIZE + 36, true)){
                    return ItemStack.EMPTY;
                }
            }
            else{
                if(!this.mergeItemStack(current, 0, MultiToolInventory.INV_SIZE, false)){
                    return ItemStack.EMPTY;
                }
            }

            if(current.getCount() == 0){
                slot.putStack(ItemStack.EMPTY);
            }
            else{
                slot.onSlotChanged();
            }

            if(current.getCount() == previous.getCount()){
                return ItemStack.EMPTY;
            }
            
            slot.onTake(playerIn, current);
        }

        return previous;
    }

    @Override
    protected boolean mergeItemStack(ItemStack stack, int startIndex, int endIndex, boolean useEndIndex) {
        boolean success = false;
        int index = startIndex;

        if(useEndIndex){
            index = endIndex - 1;
        }

        Slot slot;
        ItemStack stackinslot;

        if (stack.isStackable()) {
            while (stack.getCount() > 0 && (!useEndIndex && index < endIndex || useEndIndex && index >= startIndex)) {
                slot = this.inventorySlots.get(index);
                stackinslot = slot.getStack();

                if (!stackinslot.isEmpty() && stackinslot.getItem() == stack.getItem() && (!stack.getHasSubtypes() || stack.getMetadata() == stackinslot.getMetadata()) && ItemStack.areItemStackTagsEqual(stack, stackinslot)) {
                    int l = stackinslot.getCount() + stack.getCount();
                    int maxsize = Math.min(stack.getMaxStackSize(), slot.getItemStackLimit(stack));

                    if (l <= maxsize) {
                        stack.setCount(0);
                        stackinslot.setCount(1);
                        slot.onSlotChanged();
                        success = true;
                    } else if (stackinslot.getCount() < maxsize) {
                    	stack.setCount(stack.getCount() - stack.getMaxStackSize() - stackinslot.getCount());
                        stackinslot.setCount(stack.getMaxStackSize());
                        slot.onSlotChanged();
                        success = true;
                    }
                }

                if (useEndIndex) {
                    index--;
                } else {
                    index++;
                }
            }
        }

        if (stack.getCount() > 0) {
            if (useEndIndex) {
                index = endIndex - 1;
            } else {
                index = startIndex;
            }

            while (!useEndIndex && index < endIndex || useEndIndex && index >= startIndex && stack.getCount() > 0) {
                slot = this.inventorySlots.get(index);
                stackinslot = slot.getStack();

                if (stackinslot.isEmpty() && slot.isItemValid(stack)) {
                    if (stack.getCount() < slot.getItemStackLimit(stack)) {
                        slot.putStack(stack.copy());
                        stack.setCount(0);
                        success = true;
                        break;
                    } else {
                        ItemStack newstack = stack.copy();
                        newstack.setCount(slot.getItemStackLimit(stack));
                        slot.putStack(newstack);
                        stack.setCount(stack.getCount() - slot.getItemStackLimit(stack));
                        success = true;
                    }
                }

                if (useEndIndex) {
                    index--;
                } else {
                    index++;
                }
            }
        }

        return success;
    }
}