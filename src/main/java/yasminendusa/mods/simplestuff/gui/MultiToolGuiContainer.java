package yasminendusa.mods.simplestuff.gui;

import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.inventory.IInventory;
import net.minecraft.util.ResourceLocation;
import yasminendusa.mods.simplestuff.Constants;
import yasminendusa.mods.simplestuff.inventory.MultiToolInventory;

public class MultiToolGuiContainer extends GuiContainer {
    public MultiToolGuiContainer(IInventory playerInv, MultiToolInventory multiToolInventory) {
        super(new MultiToolContainer(playerInv, multiToolInventory));

        this.xSize = 174;
        this.ySize = 275;
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY) {
        GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
        
        this.mc.getTextureManager().bindTexture(new ResourceLocation(Constants.MODID,"textures/gui/gui_multitool.png"));
        this.drawTexturedModalRect(this.guiLeft, this.guiTop, 0, 0, 174, 166);
    
        this.mc.getTextureManager().bindTexture(new ResourceLocation(Constants.MODID,"textures/gui/gui_player_inv.png"));
        this.drawTexturedModalRect(this.guiLeft, this.guiTop + 186, 0, 0, 174, 89);
    }

    @Override
    protected void drawGuiContainerForegroundLayer(int mouseX, int mouseY) {
        String text = "Modular MultiTool";
        this.fontRenderer.drawString(text, this.xSize/2 - this.fontRenderer.getStringWidth(text) / 2, -20, 0x222222);
    }
}