package yasminendusa.mods.simplestuff.gui;

import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import yasminendusa.mods.simplestuff.items.base.ItemModuleBase;

import javax.annotation.Nullable;

public class SingleItemSlot extends Slot {
    public SingleItemSlot(IInventory inventoryIn, int index, int xPosition, int yPosition) {
        super(inventoryIn, index, xPosition, yPosition);
    }

    @Override
    public int getSlotStackLimit(){
        return 1;
    }

    @Override
    public boolean isItemValid(@Nullable ItemStack stack) {
        if(!stack.isEmpty() && stack.getItem() instanceof ItemModuleBase){
            return this.inventory.isItemValidForSlot(this.getSlotIndex(), stack);
        }

        return false;
    }
}