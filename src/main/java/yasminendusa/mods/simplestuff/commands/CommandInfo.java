package yasminendusa.mods.simplestuff.commands;

import net.minecraft.command.CommandException;
import net.minecraft.command.ICommand;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentString;
import yasminendusa.mods.simplestuff.client.render.items.ItemRenderRegister;
import yasminendusa.mods.simplestuff.items.base.ItemBase;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;

public class CommandInfo implements ICommand{
	private final List<String> aliases;
	
	public CommandInfo(){
		this.aliases = new ArrayList<>();
		this.aliases.add("ss_info");
	}
	
	@Override
	public String getName(){
		return "ss_info";
	}
	
	@Override
	public String getUsage(ICommandSender sender){
		return "ss_info";
	}
	
	@Override
	public List<String> getAliases(){
		return this.aliases;
	}
	
	@Override
	public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException{
		if(sender instanceof EntityPlayer){
			EntityPlayer player = (EntityPlayer)sender;
			ItemStack stack = player.getHeldItem(EnumHand.MAIN_HAND);
			
			sender.sendMessage(new TextComponentString(String.format("name[%s]", stack.getUnlocalizedName())));
			sender.sendMessage(new TextComponentString(String.format("meta[%d]", stack.getMetadata())));
			
			if(stack.getItem() instanceof ItemBase){
				sender.sendMessage(new TextComponentString(String.format("resloc[%s]", ((ItemBase)stack.getItem()).getResourceLocation(stack.getMetadata()))));
			}
			
			sender.sendMessage(new TextComponentString("Is model registered?: " + ItemRenderRegister.isModelRegistered(stack)));
		}
	}
	
	@Override
	public boolean checkPermission(MinecraftServer server, ICommandSender sender){
		return true;
	}
	
	@Override
	public List<String> getTabCompletions(MinecraftServer server, ICommandSender sender, String[] args, @Nullable BlockPos pos){
		return null;
	}
	
	@Override
	public boolean isUsernameIndex(String[] args, int index){
		return false;
	}
	
	@Override
	public int compareTo(ICommand o){
		return 0;
	}
}