package yasminendusa.mods.simplestuff.commands;

import com.google.common.primitives.Ints;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommand;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import org.jetbrains.annotations.NotNull;
import yasminendusa.mods.simplestuff.fakedamage.CapabilityFakeDamage;
import yasminendusa.mods.simplestuff.fakedamage.IFakeDamage;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;

public class CommandFakeRepair implements ICommand{
	private final List<String> aliases;
	
	public CommandFakeRepair(){
		this.aliases = new ArrayList<>();
		this.aliases.add("ss_repair");
	}
	
	@Override
	public String getName(){
		return "ss_repair";
	}
	
	@Override
	public String getUsage(ICommandSender sender){
		return "ss_repair [amount]";
	}
	
	@Override
	public List<String> getAliases(){
		return this.aliases;
	}
	
	@Override
	public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException{
		if(sender instanceof EntityPlayerMP){
			EntityPlayerMP player = (EntityPlayerMP)sender;
			ItemStack stack = player.getHeldItem(EnumHand.MAIN_HAND);
			
			IFakeDamage fakeDamage = stack.getCapability(CapabilityFakeDamage.FAKEDAMAGE, null);
			
			int amount = 0;
			
			if(args.length == 1){
				Integer tmpAmount = Ints.tryParse(args[0]);
				
				if(tmpAmount != null){
					amount = tmpAmount;
				}
			}
			
			if(amount == 0){
				amount = fakeDamage.getMaxDurability();
			}
			
			if(fakeDamage != null){
				fakeDamage.repair(amount);
			}
		}
	}
	
	@Override
	public boolean checkPermission(MinecraftServer server, ICommandSender sender){
		if(sender instanceof EntityPlayerMP){
			EntityPlayerMP player = (EntityPlayerMP)sender;
			boolean playerIsOp = (server.getPlayerList().getOppedPlayers().getEntry(player.getGameProfile()) != null);
			
			return player.isCreative() || server.isSinglePlayer() || playerIsOp;
		}
		
		return false;
	}
	
	@Override
	public List<String> getTabCompletions(MinecraftServer server, ICommandSender sender, String[] args, @Nullable BlockPos pos){
		return null;
	}
	
	@Override
	public boolean isUsernameIndex(String[] args, int index){
		return false;
	}
	
	@Override
	public int compareTo(@NotNull ICommand o){
		return 0;
	}
}