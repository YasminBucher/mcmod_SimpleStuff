package yasminendusa.mods.simplestuff;

import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.event.FMLServerStartingEvent;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import yasminendusa.mods.simplestuff.client.MultiToolTab;
import yasminendusa.mods.simplestuff.commands.CommandChargeEnergy;
import yasminendusa.mods.simplestuff.commands.CommandFakeRepair;
import yasminendusa.mods.simplestuff.commands.CommandInfo;
import yasminendusa.mods.simplestuff.gui.MultiToolGuiHandler;
import yasminendusa.mods.simplestuff.items.ModItems;
import yasminendusa.mods.simplestuff.proxy.CommonProxy;

@Mod(modid = Constants.MODID, version = Constants.MODVERSION)
public class SimpleStuff{
	@Mod.Instance
	public static SimpleStuff instance;
	
	@SidedProxy(clientSide = "yasminendusa.mods.simplestuff.proxy.ClientProxy", serverSide = "yasminendusa.mods.simplestuff.ServerProxy")
	public static CommonProxy proxy;
	
	@Mod.EventHandler
	public void onPreInit(FMLPreInitializationEvent event){
		MultiToolTab.init();
		ModItems.createItems();
		
		proxy.preInit(event);
	}
	
	@Mod.EventHandler
	public void onInit(FMLInitializationEvent event){
		proxy.init(event);
		
		NetworkRegistry.INSTANCE.registerGuiHandler(instance, new MultiToolGuiHandler());
	}
	
	@Mod.EventHandler
	public void onPostInit(FMLPostInitializationEvent event){
		proxy.postInit(event);
	}
	
	@Mod.EventHandler
	public void onServerStarting(FMLServerStartingEvent event){
		event.registerServerCommand(new CommandInfo());
		event.registerServerCommand(new CommandFakeRepair());
		event.registerServerCommand(new CommandChargeEnergy());
	}
}
