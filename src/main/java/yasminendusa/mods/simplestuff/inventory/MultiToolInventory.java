package yasminendusa.mods.simplestuff.inventory;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.util.text.ITextComponent;
import net.minecraftforge.common.util.Constants;
import yasminendusa.mods.simplestuff.items.modules.energymodules.ItemBatteryModule;
import yasminendusa.mods.simplestuff.items.modules.lasermodules.ItemCombatLaserModule;
import yasminendusa.mods.simplestuff.items.modules.lasermodules.ItemMiningLaserModule;
import yasminendusa.mods.simplestuff.items.modules.toolmodules.*;
import yasminendusa.mods.simplestuff.items.modules.upgrademodules.*;

import java.util.HashMap;

public class MultiToolInventory implements IInventory {
    private final ItemStack invItem;

    public static final int INV_SIZE = 16;

    private ItemStack[] inventory = new ItemStack[INV_SIZE];

    private HashMap<Integer, Class<?>> slotModuleMap = new HashMap<>();

    public MultiToolInventory(ItemStack invItem){
        this.initSlotModuleMap();

        this.invItem = invItem;

        if(!invItem.hasTagCompound()){
            invItem.setTagCompound(new NBTTagCompound());
        }

        readFromNBT(invItem.getTagCompound());
    }

    private void initSlotModuleMap(){
        slotModuleMap.put(0, ItemPickaxeModule.class);
        slotModuleMap.put(1, ItemAxeModule.class);
        slotModuleMap.put(2, ItemShovelModule.class);
        slotModuleMap.put(3, ItemSwordModule.class);
        slotModuleMap.put(4, ItemShearsModule.class);

        slotModuleMap.put(5, ItemMiningLaserModule.class);
        slotModuleMap.put(6, ItemCombatLaserModule.class);
    
        slotModuleMap.put(7, ItemBatteryModule.class);

        slotModuleMap.put(8, ItemAutosmeltModule.class);
        slotModuleMap.put(9, ItemDigAoeModule.class);
        slotModuleMap.put(10, ItemDigSpeedModule.class);
        slotModuleMap.put(11, ItemFortuneModule.class);
        slotModuleMap.put(12, ItemLaserRangeModule.class);
        slotModuleMap.put(13, ItemMiningPowerModule.class);
        slotModuleMap.put(14, ItemSilktouchModule.class);
        slotModuleMap.put(15, ItemWeaponDamageModule.class);
    }

    @Override
    public int getSizeInventory() {
        return inventory.length;
    }
    
    @Override
    public boolean isEmpty(){
        boolean isEmpty = false;
        
        for(int i = 0; i < INV_SIZE; i++){
        	if(!inventory[i].isEmpty()){
        		isEmpty = false;
        		break;
	        }
        }
        
        return isEmpty;
    }
    
    @Override
    public ItemStack getStackInSlot(int index) {
        if(inventory[index] == null){
        	inventory[index] = ItemStack.EMPTY;
        }
        
        return inventory[index];
    }
    
    @Override
    public ItemStack decrStackSize(int index, int count) {
        ItemStack stack = getStackInSlot(index);

        if(!stack.isEmpty()){
            if(stack.getCount() > count){
                stack = stack.splitStack(count);
                markDirty();
            }
            else{
                setInventorySlotContents(index, ItemStack.EMPTY);
            }
        }

        return stack;
    }
    
    @Override
    public ItemStack removeStackFromSlot(int index) {
        ItemStack stack = getStackInSlot(index);

        if(!stack.isEmpty()){
            setInventorySlotContents(index, ItemStack.EMPTY);
        }

        return stack;
    }

    @Override
    public void setInventorySlotContents(int index, ItemStack stack) {
        inventory[index] = stack;

        if(!stack.isEmpty() && stack.getCount() > getInventoryStackLimit()){
            stack.setCount(getInventoryStackLimit());
        }

        markDirty();
    }

    @Override
    public int getInventoryStackLimit() {
        return 1;
    }

    @Override
    public void markDirty() {
        writeToNBT(invItem.getTagCompound());
    }

    @Override
    public boolean isUsableByPlayer(EntityPlayer player) {
        return true;
    }

    @Override
    public void openInventory(EntityPlayer player) {
        //not implemented on purpose
    }

    @Override
    public void closeInventory(EntityPlayer player) {
        //not implemented on purpose
    }

    @Override
    public boolean isItemValidForSlot(int index, ItemStack stack) {
        return stack.getItem().getClass() == slotModuleMap.get(index);
    }

    @Override
    public int getField(int id) {
        //No fields used
        return 0;
    }

    @Override
    public void setField(int id, int value) {
        //No fields used
    }

    @Override
    public int getFieldCount() {
        //No fields used
        return 0;
    }

    @Override
    public void clear() {
        for(int i = 0; i < getSizeInventory(); i++){
            inventory[i] = ItemStack.EMPTY;
        }
    }

    @Override
    public String getName() {
        return null;
    }

    @Override
    public boolean hasCustomName() {
        return false;
    }

    @Override
    public ITextComponent getDisplayName() {
        return null;
    }

    public void readFromNBT(NBTTagCompound compound){
        if(compound != null){
            NBTTagList items = compound.getTagList("MultiToolModules", Constants.NBT.TAG_COMPOUND);

            for(int i = 0; i < items.tagCount(); i++){
                NBTTagCompound item = items.getCompoundTagAt(i);
                int slot = item.getInteger("ModuleSlot");

                if(slot >= 0 && slot < getSizeInventory()){
                    ItemStack itemStack = new ItemStack(item);

                    if(isItemValidForSlot(slot, itemStack)){
                        inventory[slot] = itemStack;
                    }
                }
            }
        }
    }

    public void writeToNBT(NBTTagCompound compound){
        NBTTagList items = new NBTTagList();

        for(int i = 0; i < getSizeInventory(); i++){
            if(!getStackInSlot(i).isEmpty()){
                NBTTagCompound item = new NBTTagCompound();
                item.setInteger("ModuleSlot", i);
                getStackInSlot(i).writeToNBT(item);

                items.appendTag(item);
            }
        }

        compound.setTag("MultiToolModules", items);
    }
}