package yasminendusa.mods.simplestuff.proxy;

import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import yasminendusa.mods.simplestuff.fakedamage.CapabilityFakeDamage;
import yasminendusa.mods.simplestuff.events.EventHandler;

public class CommonProxy{
	public void preInit(FMLPreInitializationEvent e){
		CapabilityFakeDamage.register();
	}
	
	public void init(FMLInitializationEvent e){
	
	}
	
	public void postInit(FMLPostInitializationEvent e){
		MinecraftForge.EVENT_BUS.register(new EventHandler());
	}
}