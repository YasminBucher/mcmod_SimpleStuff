package yasminendusa.mods.simplestuff.proxy;

import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import yasminendusa.mods.simplestuff.client.render.items.ItemRenderRegister;

public class ClientProxy extends CommonProxy{
	@Override
	@SideOnly(value = Side.CLIENT)
	public void preInit(FMLPreInitializationEvent e){
		super.preInit(e);
		
		ItemRenderRegister.registerItemRenderer();
	}
	
	@Override
	@SideOnly(value = Side.CLIENT)
	public void init(FMLInitializationEvent e){
		super.init(e);
	}
	
	@Override
	@SideOnly(value = Side.CLIENT)
	public void postInit(FMLPostInitializationEvent e){
		super.postInit(e);
	}
}