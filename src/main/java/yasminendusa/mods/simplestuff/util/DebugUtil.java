package yasminendusa.mods.simplestuff.util;

import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.world.World;

public class DebugUtil{
	public static void notifyPlayer(EntityPlayerMP player, World world, String msg){
		if(!world.isRemote){
			player.sendMessage(new TextComponentString(msg));
		}
	}
}