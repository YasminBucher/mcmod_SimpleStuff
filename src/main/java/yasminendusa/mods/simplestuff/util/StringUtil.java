package yasminendusa.mods.simplestuff.util;

import net.minecraft.item.ItemStack;
import net.minecraft.util.text.TextFormatting;
import yasminendusa.mods.simplestuff.items.base.*;
import yasminendusa.mods.simplestuff.items.modules.energymodules.ItemBatteryModule;

public class StringUtil{
	public static String getEnergyDisplayText(int current, int max){
		return TextFormatting.RED + String.format("Energy: %d/%d RF", current, max);
	}
	
	public static String getDurabilityDisplayText(int currentDurability, int maxDurability){
		return TextFormatting.GOLD + String.format("Durability: %d/%d", currentDurability, maxDurability);
	}
	
	public static String getModuleDisplayText(ItemStack stack){
		String displayText = "";
		
		if(stack.getItem() instanceof ItemModuleBase){
			TextFormatting formatting = TextFormatting.RESET;
			String prefix = "";
			ItemModuleBase module = (ItemModuleBase)stack.getItem();
			
			if(module instanceof ItemToolModuleBase){
				formatting = TextFormatting.GOLD;
				prefix = "[T] ";
			}
			
			if(module instanceof ItemUpgradeModuleBase){
				formatting = TextFormatting.LIGHT_PURPLE;
				prefix = "[U] ";
			}
			
			if(module instanceof ItemLaserModuleBase){
				formatting = TextFormatting.DARK_AQUA;
				prefix = "[L] ";
			}
			
			if(module instanceof ItemEnergyModuleBase){
				formatting = TextFormatting.RED;
				prefix = "[E] ";
			}
			
			displayText = formatting + prefix + module.getLocalizedName() + String.format(" Lv. %d", stack.getMetadata() + 1);
			
			if(module instanceof ItemBatteryModule){
				displayText += " - " + ((ItemBatteryModule)module).getEnergyDisplay(stack);
			}
			
			if(module instanceof ItemToolModuleBase){
				displayText += " - " + ((ItemToolModuleBase)module).getDurabilityDisplay(stack);
			}
		}
		
		return displayText;
	}
}