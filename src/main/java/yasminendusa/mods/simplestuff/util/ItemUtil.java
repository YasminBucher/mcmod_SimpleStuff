package yasminendusa.mods.simplestuff.util;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagList;

public class ItemUtil{
	public static boolean hasEnchantment(ItemStack stack, Enchantment e){
		NBTTagList ench = stack.getEnchantmentTagList();
		if(ench != null){
			for(int i = 0; i < ench.tagCount(); i++){
				short id = ench.getCompoundTagAt(i).getShort("id");
				if(id == Enchantment.getEnchantmentID(e)){
					return true;
				}
			}
		}
		return false;
	}
	
	public static void addEnchantment(ItemStack stack, Enchantment e, int level){
		if(!hasEnchantment(stack, e)){
			stack.addEnchantment(e, level);
		}
	}
	
	public static void removeEnchantment(ItemStack stack, Enchantment e){
		NBTTagList ench = stack.getEnchantmentTagList();
		if(ench != null){
			for(int i = 0; i < ench.tagCount(); i++){
				short id = ench.getCompoundTagAt(i).getShort("id");
				if(id == Enchantment.getEnchantmentID(e)){
					ench.removeTag(i);
				}
			}
			if(ench.hasNoTags() && stack.hasTagCompound()){
				stack.getTagCompound().removeTag("ench");
			}
		}
	}
}
