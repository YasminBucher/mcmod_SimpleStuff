package yasminendusa.mods.simplestuff.items;

import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.Blocks;
import net.minecraft.init.Enchantments;
import net.minecraft.item.EnumRarity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.*;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;
import net.minecraftforge.common.IShearable;
import yasminendusa.mods.simplestuff.MultiToolConfig;
import yasminendusa.mods.simplestuff.SimpleStuff;
import yasminendusa.mods.simplestuff.inventory.MultiToolInventory;
import yasminendusa.mods.simplestuff.gui.MultiToolGuiHandler;
import yasminendusa.mods.simplestuff.items.base.*;
import yasminendusa.mods.simplestuff.items.modules.energymodules.ItemBatteryModule;
import yasminendusa.mods.simplestuff.items.modules.toolmodules.ItemShearsModule;
import yasminendusa.mods.simplestuff.items.modules.toolmodules.ItemShovelModule;
import yasminendusa.mods.simplestuff.items.modules.toolmodules.ItemSwordModule;
import yasminendusa.mods.simplestuff.items.modules.upgrademodules.*;
import yasminendusa.mods.simplestuff.util.ItemUtil;
import yasminendusa.mods.simplestuff.util.StringUtil;

import javax.annotation.Nullable;
import java.util.*;

/*
* TODO: to implement
* - tool levels
* - mining power module
* - attack / weapon damage module
*
* to be implemented later:
* - laser modules
* - switch mode between standard and laser?
* */

public class ItemMultiTool extends yasminendusa.mods.simplestuff.items.base.ItemModularToolBase{
    public ItemMultiTool(String unlocalizedName) {
        super(unlocalizedName);
    }
	
	private boolean breakBlocks(ItemStack stack, BlockPos originalBlockPos, EntityPlayer player, int aoeRadius){
    	if(!this.breakBlock(stack, originalBlockPos, player)){
			return false;
		}
		
		RayTraceResult ray = this.playerRayTraceBlock(player);
		
		if(ray != null){
			EnumFacing side = ray.sideHit;
			
			BlockPos start = null;
			BlockPos end = null;
			
			if(side == EnumFacing.DOWN || side == EnumFacing.UP){
				start = originalBlockPos.add(-aoeRadius, 0, -aoeRadius);
				end = originalBlockPos.add(aoeRadius, 0, aoeRadius);
			}
			else if(side == EnumFacing.NORTH || side == EnumFacing.SOUTH){
				start = originalBlockPos.add(-aoeRadius, -aoeRadius, 0);
				end = originalBlockPos.add(aoeRadius, aoeRadius, 0);
			}
			else if(side == EnumFacing.EAST || side == EnumFacing.WEST){
				start = originalBlockPos.add(0, -aoeRadius, -aoeRadius);
				end = originalBlockPos.add(0, aoeRadius, aoeRadius);
			}
			
			if(start != null && end != null){
				for(int x = start.getX(); x <= end.getX(); x++){
					for(int y = start.getY(); y <= end.getY(); y++){
						for(int z = start.getZ(); z <= end.getZ(); z++){
							if(!(x == originalBlockPos.getX() && y == originalBlockPos.getY() && z == originalBlockPos.getZ())){
								BlockPos currentBlockPos = new BlockPos(x, y, z);
								IBlockState state = player.world.getBlockState(currentBlockPos);
								
								if(!Objects.equals(state.getBlock(), Blocks.AIR)){
									this.breakBlock(stack, currentBlockPos, player);
								}
							}
						}
					}
				}
				
				return true;
			}
		}
		
		return false;
	}
	
	private RayTraceResult playerRayTraceBlock(EntityPlayer player){
		double dist = 5.0D;
		
		if(player instanceof EntityPlayerMP){
			Minecraft.getMinecraft().playerController.getBlockReachDistance();
		}
		
		Vec3d plv = Vec3d.fromPitchYawVector(player.getPitchYaw()).scale(dist);
		Vec3d vecStart = new Vec3d(player.posX, player.posY + (double)player.getEyeHeight(), player.posZ);
		
		Vec3d vecEnd = vecStart.add(plv);
		
		return player.world.rayTraceBlocks(vecStart, vecEnd, false, true, false);
	}
	
	private boolean breakBlock(ItemStack stack, BlockPos pos, EntityPlayer player){
		IBlockState state = player.world.getBlockState(pos);
		Block block = state.getBlock();
		
		if(!this.blockCanBeBroken(stack, block)){
			return false;
		}
		
		block.removedByPlayer(state, player.world, pos, player, true);
		block.onBlockDestroyedByPlayer(player.world, pos, state);
		block.harvestBlock(player.world, player, pos, state, null, stack);
		
		ItemStack batteryModuleStack = this.getInstalledModule(stack, ItemBatteryModule.class);
		
		if(batteryModuleStack != null){
			ItemBatteryModule batteryModule = (ItemBatteryModule)batteryModuleStack.getItem();
			
			batteryModule.removeEnergy(batteryModuleStack, this.getEnergyUsedToBreakBlock(stack));
		}
		else{
			ItemStack toolModuleStack = this.getToolModuleForBlock(stack, block);
			
			if(toolModuleStack != null){
				ItemToolModuleBase toolModuleBase = (ItemToolModuleBase)toolModuleStack.getItem();
				
				toolModuleBase.applyDamage(toolModuleStack, 1);
			}
		}
		
		return true;
	}
	
	private boolean blockCanBeBroken(ItemStack stack, Block block){
		ItemStack batteryModuleStack = this.getInstalledModule(stack, ItemBatteryModule.class);
		
		if(batteryModuleStack != null){
			ItemBatteryModule batteryModule = (ItemBatteryModule)batteryModuleStack.getItem();
			
			return batteryModule.getCurrentEnergy(batteryModuleStack) >= this.getEnergyUsedToBreakBlock(stack);
		}
		else{
			ItemStack toolModuleStack = this.getToolModuleForBlock(stack, block);
			
			if(toolModuleStack != null){
				ItemToolModuleBase toolModuleBase = (ItemToolModuleBase)toolModuleStack.getItem();
				
				return toolModuleBase.getCurrentDurability(toolModuleStack) > 0;
			}
		}
		
		return true;
	}
	
	private int getEnergyUsedToBreakBlock(ItemStack stack){
		//base energy used per block
		int energyUsed = MultiToolConfig.energyUsageConfig.baseEnergyUsage;
		
		//additional energy used for silk touch
		if(this.isModuleInstalled(stack, ItemSilktouchModule.class)){
			energyUsed += MultiToolConfig.energyUsageConfig.silktouchEnergyUsage;
		}
		
		//additional energy used for autosmelt
		if(this.isModuleInstalled(stack, ItemAutosmeltModule.class)){
			energyUsed += MultiToolConfig.energyUsageConfig.autosmeltEnergyUsage;
		}
		
		//additional energy used for fortune
		ItemStack stackFortuneUpgrade = this.getInstalledModule(stack, ItemFortuneModule.class);
		
		if(stackFortuneUpgrade != null){
			int fortuneLevel = stackFortuneUpgrade.getMetadata();
			
			energyUsed += MultiToolConfig.energyUsageConfig.fortuneEnergyUsage * (fortuneLevel + 1);
		}
		
		return energyUsed;
	}
	
	private int getEnergyUsedToShearSheep(ItemStack stack){
		int energyUsed = MultiToolConfig.energyUsageConfig.shearsEnergyUsage;
		
		//additional energy used for fortune
		ItemStack stackFortuneUpgrade = this.getInstalledModule(stack, ItemFortuneModule.class);
		
		if(stackFortuneUpgrade != null){
			int fortuneLevel = stackFortuneUpgrade.getMetadata();
			
			energyUsed += MultiToolConfig.energyUsageConfig.fortuneEnergyUsage * (fortuneLevel + 1);
		}
		
		return energyUsed;
	}
	
	private List<ItemStack> getInstalledModules(ItemStack stack, Class<? extends ItemModuleBase> modClass){
		List<ItemStack> installedModules = new ArrayList<>();
		
		MultiToolInventory inventory = new MultiToolInventory(stack);
		
		for(int i = 0; i < inventory.getSizeInventory(); i++){
			ItemStack stackModule = inventory.getStackInSlot(i);
			
			if(stackModule != null && modClass.isAssignableFrom(stackModule.getItem().getClass())){
				installedModules.add(stackModule);
			}
		}
		
		return installedModules;
	}
	
	public boolean isModuleInstalled(ItemStack itemStack, Class<? extends ItemModuleBase> itemModuleClass){
		return this.getInstalledModule(itemStack, itemModuleClass) != null;
	}
	
	private ItemStack getToolModuleForBlock(ItemStack stack, Block block){
		for(ItemStack modStack : this.getInstalledModules(stack, ItemToolModuleBase.class)){
			if(((ItemToolModuleBase)modStack.getItem()).effectiveOn.contains(block)){
				return modStack;
			}
		}
		
		return null;
	}
	
	@Nullable
	public ItemStack getInstalledModule(ItemStack stack, Class<? extends ItemModuleBase> itemModuleClass){
		MultiToolInventory inventory = new MultiToolInventory(stack);
		
		for(int i = 0; i < inventory.getSizeInventory(); i++){
			ItemStack stackModule = inventory.getStackInSlot(i);
			
			if(stackModule != null && Objects.equals(stackModule.getItem().getClass(), itemModuleClass)){
				return stackModule;
			}
		}
		
		return null;
	}
	
	@Override
	public boolean isEnabled(){
		return MultiToolConfig.itemsEnabledConfig.itemMultiTool;
	}
	
	@Override
    public ActionResult<ItemStack> onItemRightClick(World world, EntityPlayer player, EnumHand hand) {
    	if(hand == EnumHand.MAIN_HAND){
		    if(!world.isRemote && player.isSneaking()){
			    player.openGui(SimpleStuff.instance, MultiToolGuiHandler.MULTITOOL_GUI, world, (int) player.posX, (int) player.posY, (int) player.posZ);
		    }
	    }

        return new ActionResult<>(EnumActionResult.PASS, player.getHeldItem(hand));
    }
	
	@Override
	public boolean itemInteractionForEntity(ItemStack stack, EntityPlayer player, EntityLivingBase target, EnumHand hand){
		ItemStack shearsModuleStack = this.getInstalledModule(stack, ItemShearsModule.class);
		ItemStack batteryModuleStack = this.getInstalledModule(stack, ItemBatteryModule.class);
		
    	if(!target.world.isRemote && target instanceof IShearable && shearsModuleStack != null){
    		boolean canShear = false;
    		ItemShearsModule shearsModule = (ItemShearsModule)shearsModuleStack.getItem();
    		
    		if(batteryModuleStack != null){
			    ItemBatteryModule batteryModule = (ItemBatteryModule)batteryModuleStack.getItem();
			    
			    if(batteryModule.getCurrentEnergy(batteryModuleStack) >= this.getEnergyUsedToShearSheep(stack)){
			    	batteryModule.removeEnergy(batteryModuleStack, this.getEnergyUsedToShearSheep(stack));
				    canShear = true;
			    }
		    }else{
    			if(shearsModule.getCurrentDurability(shearsModuleStack) > 0){
				    shearsModule.applyDamage(shearsModuleStack, 1);
				    canShear = true;
			    }
		    }
    		
		    if(canShear){
			    return shearsModule.itemInteractionForEntity(stack, player, target, hand);
		    }
	    }
    	
		return super.itemInteractionForEntity(stack, player, target, hand);
	}
	
	@Override
	public EnumActionResult onItemUse(EntityPlayer player, World world, BlockPos pos, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ){
		if(player.isSneaking()){
			return EnumActionResult.PASS;
		}
		
		ItemStack stack = player.getHeldItem(hand);
		
		ItemStack shovelModuleStack = this.getInstalledModule(stack, ItemShovelModule.class);
		ItemStack batteryModuleStack = this.getInstalledModule(stack, ItemBatteryModule.class);
		
		if(shovelModuleStack != null){
			boolean canMakePath = false;
			ItemShovelModule shovelModule = (ItemShovelModule)this.getInstalledModule(stack, ItemShovelModule.class).getItem();
			
			if(batteryModuleStack != null){
				ItemBatteryModule batteryModule = (ItemBatteryModule)batteryModuleStack.getItem();
				
				if(batteryModule.getCurrentEnergy(batteryModuleStack) >= MultiToolConfig.energyUsageConfig.shovelPathEnergyUsage){
					batteryModule.removeEnergy(batteryModuleStack, MultiToolConfig.energyUsageConfig.shovelPathEnergyUsage);
					canMakePath = true;
				}
			}else{
				if(shovelModule.getCurrentDurability(shovelModuleStack) > 0){
					shovelModule.applyDamage(shovelModuleStack, 1);
					canMakePath = true;
				}
			}
			
			if(canMakePath){
				return shovelModule.onItemUse(stack, player, world, pos, hand, facing, hitX, hitY, hitZ);
			}
		}
		
		return super.onItemUse(player, world, pos, hand, facing, hitX, hitY, hitZ);
	}
	
	@Override
    public boolean hitEntity(ItemStack stack, EntityLivingBase usedOn, EntityLivingBase user){
		float damage = MultiToolConfig.generalConfig.baseDamage;
		
		ItemStack swordModuleStack = this.getInstalledModule(stack, ItemSwordModule.class);
		
		if(swordModuleStack != null){
			damage += MultiToolConfig.generalConfig.weaponDamage;
			damage += swordModuleStack.getMetadata() * MultiToolConfig.generalConfig.weaponDamagePerLevel;
		}
		
		ItemStack weaponDamageModuleStack = this.getInstalledModule(stack, ItemWeaponDamageModule.class);
		
		if(weaponDamageModuleStack != null){
			damage += (weaponDamageModuleStack.getMetadata() + 1) * MultiToolConfig.generalConfig.weaponDamageModuleAddonDamage;
		}
		
		return usedOn.attackEntityFrom(DamageSource.causeMobDamage(user), damage);
    }

    @Override
    public float getDestroySpeed(ItemStack stack, IBlockState state){
	    if(this.blockCanBeBroken(stack, state.getBlock())){
	        List<ItemStack> installedTools = this.getInstalledModules(stack, ItemToolModuleBase.class);
		    
		    for(ItemStack toolStack : installedTools){
			    ItemToolModuleBase itemToolModuleBase = (ItemToolModuleBase)toolStack.getItem();
			
			    if(itemToolModuleBase.effectiveOn.contains(state.getBlock())){
				    return itemToolModuleBase.strVsProperBlocks;
			    }
		    }
	    }

        return 0.0F;
    }
	
	@Override
	public boolean onBlockStartBreak(ItemStack stack, BlockPos pos, EntityPlayer player){
    	boolean result = false;
    	
    	if(!player.world.isRemote){
		    if(this.isModuleInstalled(stack, ItemSilktouchModule.class)){
			    ItemUtil.addEnchantment(stack, Enchantments.SILK_TOUCH, 1);
		    }else if(this.isModuleInstalled(stack, ItemFortuneModule.class)){
		    	ItemStack fortuneModuleStack = this.getInstalledModule(stack, ItemFortuneModule.class);
		    	
			    ItemUtil.addEnchantment(stack, Enchantments.FORTUNE, fortuneModuleStack.getMetadata());
		    }
		
		    int aoeRadius = 0;
		    
		    if(this.isModuleInstalled(stack, ItemDigAoeModule.class)){
			    aoeRadius = 1;
		    }
		    
		    result = this.breakBlocks(stack, pos, player, aoeRadius);
	    }
		
		ItemUtil.removeEnchantment(stack, Enchantments.SILK_TOUCH);
		ItemUtil.removeEnchantment(stack, Enchantments.FORTUNE);
	 
		return result;
	}
	
    @Override
    public boolean canHarvestBlock(IBlockState blockIn) {
        return false;
    }

    @Override
    public boolean canHarvestBlock(IBlockState state, ItemStack stack){
		if(this.blockCanBeBroken(stack, state.getBlock())){
			List<ItemStack> installedTools = this.getInstalledModules(stack, ItemToolModuleBase.class);
			
			for(ItemStack toolStack : installedTools){
				ItemToolModuleBase itemToolModuleBase = (ItemToolModuleBase)toolStack.getItem();
				
				if(itemToolModuleBase.effectiveOn.contains(state.getBlock())){
					return this.blockCanBeBroken(stack, state.getBlock());
				}
			}
		}
        
        return false;
    }

    @Override
    public EnumRarity getRarity(ItemStack stack) {
        return EnumRarity.EPIC;
    }

    @Override
    public int getHarvestLevel(ItemStack stack, String toolClass, @Nullable EntityPlayer player, @Nullable IBlockState blockState) {
	    Block block = blockState.getBlock();
	    ItemStack toolModule = this.getToolModuleForBlock(stack, block);
	    
	    if(toolModule != null){
	    	return toolModule.getMetadata();
	    }
	    
        return super.getHarvestLevel(stack, toolClass, player, blockState);
    }

    @Override
    public Set<String> getToolClasses(ItemStack stack) {
        HashSet<String> toolClasses = new HashSet<>();
	
	    List<ItemStack> installedTools = this.getInstalledModules(stack, ItemToolModuleBase.class);
	
	    for(ItemStack toolStack : installedTools){
		    ItemToolModuleBase itemToolModuleBase = (ItemToolModuleBase)toolStack.getItem();
		
		    toolClasses.add(itemToolModuleBase.toolClass);
	    }

        return toolClasses;
    }
	
	@Override
	public void addInformation(ItemStack stack, World world, List<String> tooltip, ITooltipFlag flag){
		for(ItemStack toolMod : this.getInstalledModules(stack, ItemToolModuleBase.class)){
			tooltip.add(StringUtil.getModuleDisplayText(toolMod));
		}
		
		for(ItemStack toolMod : this.getInstalledModules(stack, ItemUpgradeModuleBase.class)){
			tooltip.add(StringUtil.getModuleDisplayText(toolMod));
		}
		
		for(ItemStack toolMod : this.getInstalledModules(stack, ItemEnergyModuleBase.class)){
			tooltip.add(StringUtil.getModuleDisplayText(toolMod));
		}
		
		for(ItemStack toolMod : this.getInstalledModules(stack, ItemLaserModuleBase.class)){
			tooltip.add(StringUtil.getModuleDisplayText(toolMod));
		}
	}
	
	@Override
	public boolean showDurabilityBar(ItemStack stack) {
		return this.isModuleInstalled(stack, ItemBatteryModule.class) && this.getDurabilityForDisplay(stack) > 0.0d;
	}
	
	@Override
    public double getDurabilityForDisplay(ItemStack stack) {
		ItemBatteryModule battery = (ItemBatteryModule)ModItems.getItemInstance(ItemBatteryModule.class);
		ItemStack batteryStack = this.getInstalledModule(stack, ItemBatteryModule.class);
		
        if(batteryStack != null && battery != null){
            return 1.0d - battery.getEnergyPercentange(batteryStack);
        }
        else{
            return 0.0d;
        }
    }
	
	@Override
	public int getDamage(ItemStack stack){
		return 0;
	}
	
	@Override
	public boolean onEntitySwing(EntityLivingBase entityLiving, ItemStack stack){
		return true;
	}
	
	@Override
	public boolean shouldCauseReequipAnimation(ItemStack oldStack, ItemStack newStack, boolean slotChanged){
		return slotChanged || false;
	}
}