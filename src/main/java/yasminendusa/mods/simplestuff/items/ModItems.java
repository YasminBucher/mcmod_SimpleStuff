package yasminendusa.mods.simplestuff.items;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import yasminendusa.mods.simplestuff.items.modules.energymodules.ItemBatteryModule;
import yasminendusa.mods.simplestuff.items.modules.lasermodules.ItemCombatLaserModule;
import yasminendusa.mods.simplestuff.items.modules.lasermodules.ItemMiningLaserModule;
import yasminendusa.mods.simplestuff.items.modules.toolmodules.*;
import yasminendusa.mods.simplestuff.items.modules.upgrademodules.*;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class ModItems {
	public static Set<Item> itemSet;

    public static void createItems(){
    	itemSet = new HashSet<>();
    	
    	itemSet.add(new ItemMultiTool("item_multitool"));
    	
    	itemSet.add(new ItemPickaxeModule("item_module_tool_pickaxe"));
	    itemSet.add(new ItemAxeModule("item_module_tool_axe"));
	    itemSet.add(new ItemShovelModule("item_module_tool_shovel"));
	    itemSet.add(new ItemSwordModule("item_module_tool_sword"));
	    itemSet.add(new ItemShearsModule("item_module_tool_shears"));
	
	    itemSet.add(new ItemCombatLaserModule("item_module_laser_combat"));
	    itemSet.add(new ItemMiningLaserModule("item_module_laser_mining"));
	
	    itemSet.add(new ItemAutosmeltModule("item_module_upgrade_autosmelt"));
	    itemSet.add(new ItemDigAoeModule("item_module_upgrade_digaoe"));
	    itemSet.add(new ItemDigSpeedModule("item_module_upgrade_digspeed"));
	    itemSet.add(new ItemLaserRangeModule("item_module_upgrade_laserrange"));
	    itemSet.add(new ItemMiningPowerModule("item_module_upgrade_miningpower"));
	    itemSet.add(new ItemSilktouchModule("item_module_upgrade_silktouch"));
	    itemSet.add(new ItemWeaponDamageModule("item_module_upgrade_weapondamage"));
	    itemSet.add(new ItemFortuneModule("item_module_upgrade_fortune"));
	
	    itemSet.add(new ItemBatteryModule("item_module_battery"));
    }
    
    public static Item getItemInstance(Class<? extends Item> itemClass){
	    for(Item item : itemSet){
	    	if(Objects.equals(item.getClass(), itemClass)){
	    		return item;
		    }
	    }
	    
	    return null;
    }
    
    public static ItemStack getItemStackForItem(Class<? extends Item> itemClass){
	    for(Item item : itemSet){
		    if(Objects.equals(item.getClass(), itemClass)){
			    return new ItemStack(item);
		    }
	    }
	
	    return ItemStack.EMPTY;
    }
}