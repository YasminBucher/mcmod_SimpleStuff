package yasminendusa.mods.simplestuff.items.base;

import net.minecraft.client.resources.I18n;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.NonNullList;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import yasminendusa.mods.simplestuff.Constants;
import yasminendusa.mods.simplestuff.client.MultiToolTab;

import java.util.List;
import java.util.Objects;

public class ItemBase extends Item{
    public ItemBase(String unlocalizedName){
        super();

        this.setUnlocalizedName(Constants.MODID + "." + unlocalizedName);
        this.setRegistryName(Constants.MODID, unlocalizedName);
        if(this.isEnabled()){ this.setCreativeTab(MultiToolTab.multiToolTab); }
        this.setHasSubtypes(true);
        
        GameRegistry.findRegistry(Item.class).register(this);
    }
    
    public String getLocalizedName(){
	    return I18n.format(this.getUnlocalizedName() + ".name");
    }
    
    public int getMaxLevel(){
    	return 0;
    }
	
    public String getResourceLocation(int level){
	    String location = Constants.MODID + ":" + this.getUnlocalizedName().substring(5 + Constants.MODID.length() + 1);
	
	    if(level > 0){
		    location += "_level" + level;
	    }
	    
	    return location;
    }
    
    public boolean isEnabled(){
    	return false;
    }
    
	@Override
	public int getMetadata(int damage){
		return damage;
	}
	
	@Override
	@SideOnly(value = Side.CLIENT)
	public void getSubItems(CreativeTabs tab, NonNullList<ItemStack> subItems){
		if(Objects.equals(this.getCreativeTab(), tab)){
			for(int i = 0; i <= this.getMaxLevel(); i++){
				ItemStack stack = new ItemStack(this, 1, i);
				subItems.add(stack);
			}
		}
	}
	
	@Override
	public void addInformation(ItemStack stack, World world, List<String> tooltip, ITooltipFlag flag){
		tooltip.add(String.format("Level %d", stack.getMetadata() + 1));
		tooltip.add("");
	}
}