package yasminendusa.mods.simplestuff.items.base;

public class ItemEnergyModuleBase extends ItemModuleBase{
	public static final int MAX_ENERGY_LV0 = 10000;
    
    public ItemEnergyModuleBase(String unlocalizedName) {
        super(unlocalizedName);
    }
	
	public int getMaxEnergy(int level){
		return (int)Math.pow(2, level) * MAX_ENERGY_LV0;
	}
	
	@Override
	public int getMaxLevel(){
		return 5;
	}
}