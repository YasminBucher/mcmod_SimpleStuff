package yasminendusa.mods.simplestuff.items.base;

public class ItemModularToolBase extends ItemBase{
	public ItemModularToolBase(String unlocalizedName){
		super(unlocalizedName);
		
		this.setNoRepair();
		this.setMaxStackSize(1);
	}
}