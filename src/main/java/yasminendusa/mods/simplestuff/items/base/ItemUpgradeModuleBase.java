package yasminendusa.mods.simplestuff.items.base;

public class ItemUpgradeModuleBase extends ItemModuleBase{
    public ItemUpgradeModuleBase(String unlocalizedName) {
        super(unlocalizedName);
    }
    
    @Override
    public int getMaxLevel(){
        return 5;
    }
}