package yasminendusa.mods.simplestuff.items.base;

public class ItemLaserModuleBase extends ItemModuleBase{
    public ItemLaserModuleBase(String unlocalizedName) {
        super(unlocalizedName);
    }
    
    @Override
    public int getMaxLevel(){
        return 5;
    }
}