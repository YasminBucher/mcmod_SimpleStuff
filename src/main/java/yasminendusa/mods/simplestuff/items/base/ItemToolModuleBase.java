package yasminendusa.mods.simplestuff.items.base;

import net.minecraft.block.Block;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;
import net.minecraftforge.common.capabilities.ICapabilityProvider;
import org.jetbrains.annotations.NotNull;
import yasminendusa.mods.simplestuff.fakedamage.CapabilityFakeDamage;
import yasminendusa.mods.simplestuff.fakedamage.IFakeDamage;
import yasminendusa.mods.simplestuff.fakedamage.MultitoolFakeDamageCapabilityProvider;
import yasminendusa.mods.simplestuff.util.StringUtil;

import java.util.Collections;
import java.util.List;
import java.util.Set;

public class ItemToolModuleBase extends ItemModuleBase{
	public static final int MAX_DURABILITY_LV0 = 64;
	
    public String toolClass = "";
    public Set<Block> effectiveOn = Collections.emptySet();
    public float strVsProperBlocks = 0.0F;

    public ItemToolModuleBase(String unlocalizedName, String toolClass, Set<Block> effectiveOn, float strVsProperBlocks) {

    	
        super(unlocalizedName);

        this.toolClass = toolClass;
        this.effectiveOn = effectiveOn;
        this.strVsProperBlocks = strVsProperBlocks;
    }
	
	public int getMaxDurability(int level){
		return (int)Math.pow(2, level) * MAX_DURABILITY_LV0;
	}
	
	public int getCurrentDurability(ItemStack stack){
		if(stack.hasCapability(CapabilityFakeDamage.FAKEDAMAGE, null)){
			IFakeDamage fakeDamage = stack.getCapability(CapabilityFakeDamage.FAKEDAMAGE, null);
			
			if(fakeDamage != null){
				return fakeDamage.getDurability();
			}
		}
		
		return 0;
	}
	
	public double getDurabilityPercentage(ItemStack stack){
		return Math.min((double)this.getCurrentDurability(stack) / (double)this.getMaxDurability(stack.getMetadata()), 1.0d);
	}
	
	public String getDurabilityDisplay(ItemStack stack){
		return StringUtil.getDurabilityDisplayText(this.getCurrentDurability(stack), this.getMaxDurability(stack.getMetadata()));
	}
	
	public void applyDamage(ItemStack stack, int damage){
		if(stack.hasCapability(CapabilityFakeDamage.FAKEDAMAGE, null)){
			IFakeDamage fakeDamage = stack.getCapability(CapabilityFakeDamage.FAKEDAMAGE, null);
			
			if(fakeDamage != null){
				fakeDamage.applyDamage(damage);
			}
		}
	}
	
	public void repair(ItemStack stack, int repairAmount){
		//TODO what to use to repair? and how? since these items aren't repairable the default way, do i need own recipes or even my own block?
		if(stack.hasCapability(CapabilityFakeDamage.FAKEDAMAGE, null)){
			IFakeDamage fakeDamage = stack.getCapability(CapabilityFakeDamage.FAKEDAMAGE, null);
			
			if(fakeDamage != null){
				fakeDamage.repair(repairAmount);
			}
		}
	}
	
	@Override
	public int getMaxLevel(){
		return 5;
	}
	
	@Override
	public void addInformation(ItemStack stack, World world, List<String> tooltip, ITooltipFlag flag){
    	super.addInformation(stack, world, tooltip, flag);
		
		tooltip.add(this.getDurabilityDisplay(stack));
	}
	
	@Override
	public boolean showDurabilityBar(ItemStack stack){
		return this.getDurabilityPercentage(stack) < 1.0d;
	}
	
	@Override
	public double getDurabilityForDisplay(ItemStack stack){
		return 1.0d - this.getDurabilityPercentage(stack);
	}
	
	@Override
	public @NotNull ICapabilityProvider initCapabilities(ItemStack stack, NBTTagCompound nbt){
		return new MultitoolFakeDamageCapabilityProvider(stack, this);
	}
}