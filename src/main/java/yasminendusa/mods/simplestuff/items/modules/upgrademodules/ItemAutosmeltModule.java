package yasminendusa.mods.simplestuff.items.modules.upgrademodules;

import yasminendusa.mods.simplestuff.MultiToolConfig;
import yasminendusa.mods.simplestuff.items.base.ItemUpgradeModuleBase;

public class ItemAutosmeltModule extends ItemUpgradeModuleBase{
    public ItemAutosmeltModule(String unlocalizedName) {
        super(unlocalizedName);
    }
	
	@Override
	public boolean isEnabled(){
		return MultiToolConfig.itemsEnabledConfig.itemAutosmeltModule;
	}
	
	@Override
	public int getMaxLevel(){
		return 0;
	}
}