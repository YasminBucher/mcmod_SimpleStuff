package yasminendusa.mods.simplestuff.items.modules.upgrademodules;

import yasminendusa.mods.simplestuff.MultiToolConfig;
import yasminendusa.mods.simplestuff.items.base.ItemUpgradeModuleBase;

public class ItemFortuneModule extends ItemUpgradeModuleBase{
    public ItemFortuneModule(String unlocalizedName) {
        super(unlocalizedName);
    }
	
	@Override
	public boolean isEnabled(){
		return MultiToolConfig.itemsEnabledConfig.itemFortuneModule;
	}
	
	@Override
	public int getMaxLevel(){
		return 2;
	}
}