package yasminendusa.mods.simplestuff.items.modules.upgrademodules;

import yasminendusa.mods.simplestuff.MultiToolConfig;
import yasminendusa.mods.simplestuff.items.base.ItemUpgradeModuleBase;

public class ItemSilktouchModule extends ItemUpgradeModuleBase{
    public ItemSilktouchModule(String unlocalizedName) {
        super(unlocalizedName);
    }
	
	@Override
	public boolean isEnabled(){
		return MultiToolConfig.itemsEnabledConfig.itemSilktouchModule;
	}
	
	@Override
	public int getMaxLevel(){
		return 0;
	}
}