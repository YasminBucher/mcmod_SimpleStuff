package yasminendusa.mods.simplestuff.items.modules.toolmodules;

import com.google.common.collect.Sets;
import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import yasminendusa.mods.simplestuff.MultiToolConfig;
import yasminendusa.mods.simplestuff.items.base.ItemToolModuleBase;

import java.util.Set;

public class ItemSwordModule extends ItemToolModuleBase{
    private static final Set<Block> EFFECTIVEON = Sets.newHashSet(Blocks.WEB);

    public ItemSwordModule(String unlocalizedName) {
        super(unlocalizedName, "sword", EFFECTIVEON, 5.0F);
    }
    
    @Override
    public boolean isEnabled(){
        return MultiToolConfig.itemsEnabledConfig.itemSwordModule;
    }
}