package yasminendusa.mods.simplestuff.items.modules.energymodules;

import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ActionResult;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumHand;
import net.minecraft.world.World;
import net.minecraftforge.common.capabilities.ICapabilityProvider;
import net.minecraftforge.energy.CapabilityEnergy;
import net.minecraftforge.energy.IEnergyStorage;
import yasminendusa.mods.simplestuff.MultiToolConfig;
import yasminendusa.mods.simplestuff.energy.MultitoolEnergyCapabilityProvider;
import yasminendusa.mods.simplestuff.items.base.ItemEnergyModuleBase;
import yasminendusa.mods.simplestuff.util.StringUtil;

import java.util.List;

public class ItemBatteryModule extends ItemEnergyModuleBase{
	private static final int MAX_ENERGY_LV0 = 10000;
	
    public ItemBatteryModule(String unlocalizedName) {
        super(unlocalizedName);
    }
    
    public double getEnergyPercentange(ItemStack stack)
    {
    	return Math.min((double)this.getCurrentEnergy(stack) / (double)this.getMaxEnergy(stack.getMetadata()), 1.0d);
    }
	
    public String getEnergyDisplay(ItemStack stack){
    	return StringUtil.getEnergyDisplayText(this.getCurrentEnergy(stack), this.getMaxEnergy(stack.getMetadata()));
    }
	
	@Override
	public int getMaxEnergy(int level){
		return (int)Math.pow(2, level) * MAX_ENERGY_LV0;
	}
    
    public int getCurrentEnergy(ItemStack stack){
    	if(stack.hasCapability(CapabilityEnergy.ENERGY, null)){
    		IEnergyStorage energyStorage = stack.getCapability(CapabilityEnergy.ENERGY, null);
    		
    		if(energyStorage != null){
    			return energyStorage.getEnergyStored();
		    }
	    }
	    
	    return 0;
    }
    
    public void addEnergy(ItemStack stack, int energy){
    	if(stack.hasCapability(CapabilityEnergy.ENERGY, null)){
    		IEnergyStorage energyStorage = stack.getCapability(CapabilityEnergy.ENERGY, null);
    		
    		if(energyStorage != null){
    			energyStorage.receiveEnergy(energy, false);
		    }
	    }
    }
    
    public void removeEnergy(ItemStack stack, int energy){
    	if(stack.hasCapability(CapabilityEnergy.ENERGY, null)){
    		IEnergyStorage energyStorage = stack.getCapability(CapabilityEnergy.ENERGY, null);
    		
    		if(energyStorage != null){
    			energyStorage.extractEnergy(energy, false);
		    }
	    }
    }
	
	@Override
	public boolean isEnabled(){
		return MultiToolConfig.itemsEnabledConfig.itemBatteryModule;
	}
	
	@Override
	public void addInformation(ItemStack stack, World world, List<String> tooltip, ITooltipFlag flag){
		super.addInformation(stack, world, tooltip, flag);
		
		tooltip.add(this.getEnergyDisplay(stack));
	}
	
	@Override
	public boolean showDurabilityBar(ItemStack stack) {
		return this.getEnergyPercentange(stack) < 1.0d;
	}
	
	@Override
	public double getDurabilityForDisplay(ItemStack stack) {
		return 1.0d - this.getEnergyPercentange(stack);
	}
	
	@Override
	public ICapabilityProvider initCapabilities(ItemStack stack, NBTTagCompound nbt){
		return new MultitoolEnergyCapabilityProvider(stack, this);
	}
}