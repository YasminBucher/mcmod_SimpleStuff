package yasminendusa.mods.simplestuff.items.modules.toolmodules;

import com.google.common.collect.Sets;
import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import yasminendusa.mods.simplestuff.MultiToolConfig;
import yasminendusa.mods.simplestuff.items.base.ItemToolModuleBase;

import java.util.Set;

public class ItemAxeModule extends ItemToolModuleBase{
    private static final Set<Block> EFFECTIVEON = Sets.newHashSet(Blocks.PLANKS, Blocks.BOOKSHELF, Blocks.LOG, Blocks.LOG2, Blocks.CHEST, Blocks.PUMPKIN, Blocks.LIT_PUMPKIN, Blocks.MELON_BLOCK, Blocks.LADDER, Blocks.WOODEN_BUTTON, Blocks.WOODEN_PRESSURE_PLATE);

    public ItemAxeModule(String unlocalizedName) {
        super(unlocalizedName, "axe", EFFECTIVEON, 5.0F);
    }
    
    @Override
    public boolean isEnabled(){
        return MultiToolConfig.itemsEnabledConfig.itemAxeModule;
    }
}