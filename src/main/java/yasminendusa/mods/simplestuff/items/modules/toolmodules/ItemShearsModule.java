package yasminendusa.mods.simplestuff.items.modules.toolmodules;

import com.google.common.collect.Sets;
import net.minecraft.block.Block;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.init.Enchantments;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.common.IShearable;
import yasminendusa.mods.simplestuff.MultiToolConfig;
import yasminendusa.mods.simplestuff.items.base.ItemToolModuleBase;

import java.util.List;
import java.util.Set;

public class ItemShearsModule extends ItemToolModuleBase{
    private static final Set<Block> EFFECTIVEON = Sets.newHashSet(Blocks.LEAVES, Blocks.WEB, Blocks.TALLGRASS, Blocks.VINE, Blocks.TRIPWIRE, Blocks.WOOL, Blocks.CARPET, Blocks.DEADBUSH);

    public ItemShearsModule(String unlocalizedName) {
        super(unlocalizedName, "shears", EFFECTIVEON, 5.0F);
    }
	
	public boolean itemInteractionForEntity(ItemStack stack, EntityPlayer player, EntityLivingBase target, EnumHand hand){
		IShearable shearableTarget = (IShearable)target;
		BlockPos pos = new BlockPos(target.posX, target.posY, target.posZ);
		
		if(shearableTarget.isShearable(stack, target.world, pos)){
			List<ItemStack> drops = shearableTarget.onSheared(stack, target.world, pos, EnchantmentHelper.getEnchantmentLevel(Enchantments.FORTUNE, stack));
			
			for(ItemStack drop : drops)
			{
				target.entityDropItem(drop, 1.0F);
			}
		}
		
		return true;
	}
	
	@Override
	public boolean isEnabled(){
		return MultiToolConfig.itemsEnabledConfig.itemShearsModule;
	}
}
