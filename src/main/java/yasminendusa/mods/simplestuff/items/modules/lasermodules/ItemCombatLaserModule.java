package yasminendusa.mods.simplestuff.items.modules.lasermodules;

import yasminendusa.mods.simplestuff.MultiToolConfig;
import yasminendusa.mods.simplestuff.items.base.ItemLaserModuleBase;

public class ItemCombatLaserModule extends ItemLaserModuleBase{
    public ItemCombatLaserModule(String unlocalizedName) {
        super(unlocalizedName);
    }
    
    @Override
    public boolean isEnabled(){
        return MultiToolConfig.itemsEnabledConfig.itemCombatLaserModule;
    }
}
