package yasminendusa.mods.simplestuff.items.modules.lasermodules;

import yasminendusa.mods.simplestuff.MultiToolConfig;
import yasminendusa.mods.simplestuff.items.base.ItemLaserModuleBase;

public class ItemMiningLaserModule extends ItemLaserModuleBase{
    public ItemMiningLaserModule(String unlocalizedName) {
        super(unlocalizedName);
    }
    
    @Override
    public boolean isEnabled(){
        return MultiToolConfig.itemsEnabledConfig.itemMiningLaserModule;
    }
}
