package yasminendusa.mods.simplestuff.items.modules.upgrademodules;

import yasminendusa.mods.simplestuff.MultiToolConfig;
import yasminendusa.mods.simplestuff.items.base.ItemUpgradeModuleBase;

public class ItemWeaponDamageModule extends ItemUpgradeModuleBase{
    public ItemWeaponDamageModule(String unlocalizedName) {
        super(unlocalizedName);
    }
    
    @Override
    public boolean isEnabled(){
        return MultiToolConfig.itemsEnabledConfig.itemWeaponDamageModule;
    }
}