package yasminendusa.mods.simplestuff.items.modules.upgrademodules;

import yasminendusa.mods.simplestuff.MultiToolConfig;
import yasminendusa.mods.simplestuff.items.base.ItemUpgradeModuleBase;

public class ItemLaserRangeModule extends ItemUpgradeModuleBase{
    public ItemLaserRangeModule(String unlocalizedName) {
        super(unlocalizedName);
    }
    
    @Override
    public boolean isEnabled(){
        return MultiToolConfig.itemsEnabledConfig.itemLaserRangeModule;
    }
}