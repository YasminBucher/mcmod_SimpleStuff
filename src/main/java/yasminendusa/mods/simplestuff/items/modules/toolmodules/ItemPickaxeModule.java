package yasminendusa.mods.simplestuff.items.modules.toolmodules;

import com.google.common.collect.Sets;
import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import yasminendusa.mods.simplestuff.MultiToolConfig;
import yasminendusa.mods.simplestuff.items.base.ItemToolModuleBase;

import java.util.Set;

public class ItemPickaxeModule extends ItemToolModuleBase{
    private static final Set<Block> EFFECTIVEON = Sets.newHashSet(Blocks.ACTIVATOR_RAIL, Blocks.COAL_ORE, Blocks.COBBLESTONE, Blocks.DETECTOR_RAIL, Blocks.DIAMOND_BLOCK, Blocks.DIAMOND_ORE, Blocks.DOUBLE_STONE_SLAB, Blocks.GOLDEN_RAIL, Blocks.GOLD_BLOCK, Blocks.GOLD_ORE, Blocks.ICE, Blocks.IRON_BLOCK, Blocks.IRON_ORE, Blocks.LAPIS_BLOCK, Blocks.LAPIS_ORE, Blocks.LIT_REDSTONE_ORE, Blocks.MOSSY_COBBLESTONE, Blocks.NETHERRACK, Blocks.PACKED_ICE, Blocks.RAIL, Blocks.REDSTONE_ORE, Blocks.SANDSTONE, Blocks.RED_SANDSTONE, Blocks.STONE, Blocks.STONE_SLAB, Blocks.STONE_BUTTON, Blocks.STONE_PRESSURE_PLATE);

    public ItemPickaxeModule(String unlocalizedName) {
        super(unlocalizedName, "pickaxe", EFFECTIVEON, 5.0F);
    }
    
    @Override
    public boolean isEnabled(){
        return MultiToolConfig.itemsEnabledConfig.itemPickaxeModule;
    }
}