package yasminendusa.mods.simplestuff.items.modules.upgrademodules;

import yasminendusa.mods.simplestuff.MultiToolConfig;
import yasminendusa.mods.simplestuff.items.base.ItemUpgradeModuleBase;

public class ItemDigAoeModule extends ItemUpgradeModuleBase{
    public ItemDigAoeModule(String unlocalizedName) {
        super(unlocalizedName);
    }
	
	@Override
	public boolean isEnabled(){
		return MultiToolConfig.itemsEnabledConfig.itemDigAoeModule;
	}
	
	@Override
	public int getMaxLevel(){
		return 0;
	}
}