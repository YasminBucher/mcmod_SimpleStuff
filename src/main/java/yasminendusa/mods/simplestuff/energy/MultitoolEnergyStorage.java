package yasminendusa.mods.simplestuff.energy;

import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.energy.IEnergyStorage;
import yasminendusa.mods.simplestuff.items.ModItems;
import yasminendusa.mods.simplestuff.items.base.ItemEnergyModuleBase;

public class MultitoolEnergyStorage implements IEnergyStorage{
	ItemStack stack;
	ItemEnergyModuleBase item;
	
	public MultitoolEnergyStorage(ItemStack stack, ItemEnergyModuleBase item){
		this.stack = stack;
		this.item = item;
	}
	
	public int getCurrentEnergy(){
		if(stack.hasTagCompound()){
			int energy = stack.getTagCompound().getInteger("currentEnergy");
			
			return energy;
		}
		
		return 0;
	}
	
	public void setCurrentEnergy(int currentEnergy){
		if(!stack.hasTagCompound()){
			stack.setTagCompound(new NBTTagCompound());
		}
		
		stack.getTagCompound().setInteger("currentEnergy", currentEnergy);
	}
	
	@Override
	public int receiveEnergy(int maxReceive, boolean simulate){
		int actuallyReceived = Math.min(this.getMaxEnergyStored() - this.getCurrentEnergy(), maxReceive);
		
		if(!simulate){
			this.setCurrentEnergy(Math.max(this.getCurrentEnergy() + actuallyReceived, this.getMaxEnergyStored()));
		}
		
		return actuallyReceived;
	}
	
	@Override
	public int extractEnergy(int maxExtract, boolean simulate){
		int actuallyExtracted = Math.min(maxExtract, this.getCurrentEnergy());
		
		if(!simulate){
			this.setCurrentEnergy(Math.max(this.getCurrentEnergy() - maxExtract, 0));
		}
		
		return actuallyExtracted;
	}
	
	@Override
	public int getEnergyStored(){
		return this.getCurrentEnergy();
	}
	
	@Override
	public int getMaxEnergyStored(){
		ItemEnergyModuleBase itemEnergyModuleBase = (ItemEnergyModuleBase)ModItems.getItemInstance(this.item.getClass());
		
		return itemEnergyModuleBase.getMaxEnergy(this.stack.getMetadata());
	}
	
	@Override
	public boolean canExtract(){
		return false;
	}
	
	@Override
	public boolean canReceive(){
		return true;
	}
}