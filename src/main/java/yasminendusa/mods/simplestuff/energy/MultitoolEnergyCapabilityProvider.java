package yasminendusa.mods.simplestuff.energy;

import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumFacing;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.ICapabilityProvider;
import net.minecraftforge.energy.CapabilityEnergy;
import yasminendusa.mods.simplestuff.items.base.ItemEnergyModuleBase;

import javax.annotation.Nullable;

public class MultitoolEnergyCapabilityProvider implements ICapabilityProvider{
	public MultitoolEnergyStorage energyStorage;
	
	public MultitoolEnergyCapabilityProvider(ItemStack stack, ItemEnergyModuleBase item){
		this.energyStorage = new MultitoolEnergyStorage(stack, item);
	}
	
	@Override
	public boolean hasCapability(Capability<?> capability, @Nullable EnumFacing facing){
		if(capability == CapabilityEnergy.ENERGY){
			return true;
		}
		
		return false;
	}
	
	@Override
	public <T> T getCapability(Capability<T> capability, @Nullable EnumFacing facing){
		if(capability == CapabilityEnergy.ENERGY){
			return (T) this.energyStorage;
		}
		
		return null;
	}
}