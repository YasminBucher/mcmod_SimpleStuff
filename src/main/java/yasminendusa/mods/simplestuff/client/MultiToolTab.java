package yasminendusa.mods.simplestuff.client;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import yasminendusa.mods.simplestuff.items.ItemMultiTool;
import yasminendusa.mods.simplestuff.items.ModItems;

public class MultiToolTab extends CreativeTabs {
    public static MultiToolTab multiToolTab;

    public static void init(){
        multiToolTab = new MultiToolTab();
    }

    public MultiToolTab(){
        super("ModularMultiTool");
    }

    @SideOnly(value = Side.CLIENT)
    @Override
    public ItemStack getTabIconItem() {
        return ModItems.getItemStackForItem(ItemMultiTool.class);
    }
}