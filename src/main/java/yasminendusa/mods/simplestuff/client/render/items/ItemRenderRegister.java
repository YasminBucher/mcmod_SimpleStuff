package yasminendusa.mods.simplestuff.client.render.items;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.ItemModelMesher;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.client.model.ModelLoader;
import yasminendusa.mods.simplestuff.Constants;
import yasminendusa.mods.simplestuff.items.ModItems;
import yasminendusa.mods.simplestuff.items.base.ItemBase;

public class ItemRenderRegister{
	public static void registerItemRenderer(){
		for(Item item : ModItems.itemSet){
			if(item instanceof ItemBase){
				ItemBase itemBase = (ItemBase)item;
				
				for(int i = 0; i <= itemBase.getMaxLevel(); i++){
					register(itemBase, i);
				}
			}
			else{
				register(item);
			}
		}
	}
	
	private static void register(ItemBase item, int level){
		String location = item.getResourceLocation(level);
		
		ModelLoader.setCustomModelResourceLocation(item, level, new ModelResourceLocation(location, "inventory"));
	}
	
	private static void register(Item item){
		String location = Constants.MODID + ":" + item.getUnlocalizedName().substring(5 + 17);
		
		ModelLoader.setCustomModelResourceLocation(item, 0, new ModelResourceLocation(location, "inventory"));
	}
	
	public static boolean isModelRegistered(ItemStack stack){
		ItemModelMesher itemModelMesher = Minecraft.getMinecraft().getRenderItem().getItemModelMesher();
		
		return itemModelMesher.getItemModel(stack) != null;
	}
}